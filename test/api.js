//instanciar los paquetes instalados
var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
var server = require('../server');

chai.use(chaiHttp) // Configurar chai con el mòdulo http

//Definir conjunto de pruebas
//funciones lamba Dado x ejecuta y
describe('Test de conectividad',() => {
  it('Google Funciona', (done) => {
    // body...
    chai.request('https://www.google.com').get('/').end((err, res) => {
      //body..
      //console.log(res)
      res.should.have.status(200)
      done()
      });
  });
});

describe('Test de API Usuarios',() => {
  it('Raíz de la API contesta', (done) => {
    // body...
    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
      //body..
      //console.log(res)
      res.should.have.status(200)
      done()
      });
  });
  it('Raíz de la API funciona', (done) => {
    // body...
    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
      //body..
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array')
      done()
      });
  });

  it('Raíz de la API devuelve 4 colecciones', (done) => {
    // body...
    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
      //body..
      //console.log(res)
      //console.log(res.body)
      res.should.have.status(200)
      res.body.should.be.a('array')
      res.body.length.should.be.eql(4)
      done()
      });
  });

  it('Raíz de la API devuelve los objetos correctos', (done) => {
    // body...
    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
      //body..
      //console.log(res)
      //console.log(res.body)
      res.should.have.status(200)
      res.body.should.be.a('array')
      res.body.length.should.be.eql(4)
      for (var i = 0; i < res.body.length; i++) {
        res.body[i].should.have.property('recurso')
        res.body[i].should.have.property('url')
      }
      done()
      });
  });
});

describe('Test de API Movimientos',() => {
  it('Raíz de la API movimientos contesta', (done) => {
    // body...
    chai.request('http://localhost:3000').get('/v3/movimientos').end((err, res) => {
      //body..
      //console.log(res)
      res.should.have.status(200)
      done()
      });
  });

  it('Raíz de la API movimientos contesta con array', (done) => {
    // body...
    chai.request('http://localhost:3000').get('/v3/movimientos').end((err, res) => {
      //body..
      //console.log(res)
      //console.log(res.body)
      res.should.have.status(200)
      res.body.should.be.a('array')
      //res.body.length.should.be.eql(4)
      /*for (var i = 0; i < res.body.length; i++) {
        res.body[i].should.have.property('idcuenta')
        res.body[i].should.have.property('usuario')
        res.body[i].should.have.property('saldo')
        res.body[i].should.have.property('pais')
      }*/
      done()
      });
  });

  it('Raíz de la API movimientos con bùsqueda por id', (done) => {
    // body...
    chai.request('http://localhost:3000').get('/v3/movimientos:id').end((err, res) => {
      //body..
      //console.log(res)
      //console.log(res.body)
      res.should.have.status(200)
      done()
      });
  });

  it('Raíz de la API movimientos con bùsqueda por id con array', (done) => {
    // body...
    chai.request('http://localhost:3000').get('/v3/movimientos:id').end((err, res) => {
      //body..
      //console.log(res)
      //console.log(res.body)
      res.should.have.status(200)
      res.body.should.be.a('array')
      //res.body.length.should.be.eql(4)
      for (var i = 0; i < res.body.length; i++) {
        res.body[i].should.have.property('recurso')
        res.body[i].should.have.property('url')
      }
      done()
      });
  });

  it('Raíz de la API movimientos con bùsqueda por id con array de un id especìfico', (done) => {
    // body...
    chai.request('http://localhost:3000').get('/v3/movimientos:id').end((err, res) => {
      //body..
      //console.log(res)
      //console.log(res.body)
      res.should.have.status(200)
      res.body.should.be.a('array')
      //res.body.length.should.be.eql(4)
      for (var i = 0; i < res.body.length; i++) {
        res.body[i].should.have.property('recurso')
        res.body[i].should.have.property('url')
        //res.body[i].should.have.property('url')
      }
      done()
      });
  });

});
