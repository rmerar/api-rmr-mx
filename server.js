console.log("Aquí funcionando nodemon");

var movimientosJSON = require('./movimientosv2.json');
var usuariosJSON = require('./usuarios.json');
var express = require ('express');
var bodyparser = require ('body-parser');
var jsonQuery = require('json-query');
var requestJson = require('request-json');

var app = express();
app.use(bodyparser.json());

app.get('/', function (req , res){
  res.send('Hola API');
});

app.get('/v1/movimientos', function (req,res){
  //res.send('mov1,mov2,mov3');
  res.sendfile('movimientosv1.json');
    });

app.get('/v2/movimientos', function (req,res){
  //res.send('mov1,mov2,mov3');
  //res.sendfile('movimientosv2.json');
  res.send(movimientosJSON);
  });

app.get('/v2/movimientos/:id',function(req,res) {
    console.log(req.params.id);
    console.log(movimientosJSON);
    //res.send('Ha solicitado consultar el movimiento: '+req.params.id);
    //sendfile is deprecated
    //res.sendfile('movimientosv2.json');
    res.send(movimientosJSON[req.params.id-1]);
});

app.get('/v2/movimientosq/',function(req,res) {
    console.log(req.query);
    res.send ("Recibido");
        });
app.get('/v2/movimientosp/:id/:nombre',function(req,res) {
    console.log(req.params);
    res.send ("Recibido");
        });

app.post ('/v2/movimientos', function (req,res){
  console.log (req.headers);
  if (req.headers['authorization'] != undefined )
  {
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send ("Movimiento dado de alta");
  } else   {
  res.send ("No esta autorizado");
}
});
app.put ('/v2/movimientos/:id', function (req,res){
  /*var modificar = req.body;
  movimientosJSON[req.params.id-1] = modificar;
  res.send ("Movimiento modificado");*/
  var modificar = req.body;
  var actual = movimientosJSON[req.params.id-1]
  if (modificar.importe != undefined)
  {
    actual.importe = modificar.importe;
    }
    if (modificar.ciudad != undefined)
    {
      actual.ciudad = modificar.ciudad;
      }
  res.send ("Movimiento modificado");
});

app.delete ('/v2/movimientos/:id', function (req,res){
  //console.log (req);
  var actual = movimientosJSON[req.params.id - 1];
  movimientosJSON.push({
    "id": movimientosJSON.length + 1,
    "ciudad": actual.ciudad,
    "importe":actual.importe * (-1),
    "concepto":"Negativo del " + req.params.id
});
res.send ("Movimiento anulado");
});

app.get('/v2/usuarios/:id', function (req,res){
  console.log(req.params.id);
  console.log(usuariosJSON);
  res.send(usuariosJSON[req.params.id-1]);
  res.send ("Id usuario Recibido");
  });

app.post('/v2/usuarios/login', function (req,res){
  var email = req.headers ['email']

    var password = req.headers ['password']

    var resultados = jsonQuery('[email=' + email + ']', {data:usuariosJSON})

    if(resultados.value != null && resultados.value.password == password){

      usuariosJSON[resultados.value.id-1].estado='logged'

      res.send('{"login":"OK"}')

    }else {

      res.send('{"login":"ERROR"}')

    }

/*    var email = req.headers['email'];
    var pwd = req.headers['password'];
    if (email != undefined && pwd != undefined)
    {
    console.log(email,pwd);
    for (var i = 0; i < usuariosJSON.length; i++)
    {
      if (usuariosJSON[i].email == email && usuariosJSON[i].password == pwd ){
        console.log(email,pwd + "Valor de array" + usuariosJSON[i].email + usuariosJSON[i].password);
        var actual = usuariosJSON[i].id;
        console.log("Valor de array ID" + usuariosJSON[i].id);
        usuariosJSON[i].push({
          "id": actual.id,
          "email": actual.email,
          "password": actual.password,
          "nombre": actual.nombre,
          "estado": "En sesión"
          //res.send ('{"Usuario en sesiòn"}');
        });}else {
          res.send ("No existe el usuario con las credenciales");
        }

    }
    } else {
    res.send ("No cuentas con credenciales válidas");
  }*/
  });

  app.post('/v2/usuarios/logout', function (req,res){
    var email = req.headers ['email']
    var password = req.headers ['password']
    var resultados = jsonQuery('[email=' + email + ']', {data:usuariosJSON})
    if(resultados.value != null && resultados.value.password == password && resultados.value.estado == 'logged'){
        usuariosJSON[resultados.value.id-1].estado='logout'
        res.send('{"logout":"OK"}')
      }else {

        res.send('{"logout":"ERROR"}')

      }
});

// Versión 3 de la API conectada a MLab
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techurmrmx/collections";
var apiKey = "apiKey=gS9i-CqT9PtqBQlJ_vIgp1LF6PR6_Ttx";
var clienteMlab = requestJson.createClient(urlMlabRaiz + "?" + apiKey);

app.get('/v3', function (req, res){
    clienteMlab.get('',function(err, resM, body){
      var colleccionesUsuario = [] ;
      if(!err){
        for (var i = 0; i < body.length; i++) {
          if (body[i] != "system.indexes" && "objectlabs-system" && "objectlabs-system.admin.collections"){
            //colleccionesUsuario.push(body[i]);
            colleccionesUsuario.push ({"recurso":body[i], "url":"/v3/" + body[i]})
          }
        }
      res.send(colleccionesUsuario);
      }
      else {
        res.send(err);
        }
    });
  });

  app.get('/v3/usuarios', function (req, res){
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey);
    clienteMlab.get('', function (err, resM, body){
      res.send(body);
    });
    });

  app.get('/v3/usuarios/:id', function (req, res){
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?");
    clienteMlab.get('?q={"id":' + req.params.id + '}&' + apiKey,
    function (err, resM, body){
      res.send(body);
    });
    });

  app.post('/v3/usuarios', function (req, res){
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey);
    clienteMlab.post('',req.body, function (err, resM, body){
      res.send(body);
    });
    });

  app.put('/v3/usuarios/:id', function(req, res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
    clienteMlab.put('?q={"id": ' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
      res.send(body)
  })
})

app.delete('/v3/usuarios/:id', function (req, res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?");
  clienteMlab.delete('?q={"id":' + req.params.id + '}&' + apiKey,
  function (err, resM, body){
    res.send(body);
  });
  });

app.get('/v3/movimientos', function (req, res){
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos?" + apiKey);
    clienteMlab.get('', function (err, resM, body){
      res.send(body);
    });
    });

  app.get('/v3/movimientos/:id', function (req, res){
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos?");
    clienteMlab.get('?q={"idcuenta":' + req.params.id + '}&' + apiKey,
    function (err, resM, body){
      res.send(body);
    });
    });



app.listen(3000);
console.log("Escuchando en el puerto 3000");
